# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is a sample controller
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

# ---- example index page ----
def index():
    response.flash = T("BIENVENIDO A NUESTRO SITIO")
    return dict(message=" ")
def nosotros():
    response.flash = T("BIENVENIDO A NUESTRO SITIO")
    return dict()
def juegoUno():
    return dict()
def juegoDos():
    return dict()
def juegoTres():
    return dict()
def puntos():
    pts= request.vars["puntaje"]
    game = request.vars["game"]
    nombre = request.vars["nombre"]
    if (pts != None):
        db.puntaje.insert(puntos=int(pts),juego=int(game),jugador=nombre)
    consulta = db.executesql("SELECT puntos,juego FROM puntaje ORDER BY puntos DESC")
    return dict(reg = consulta)
