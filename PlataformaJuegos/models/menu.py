# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# ----------------------------------------------------------------------------------------------------------------------
# this is the main application menu add/remove items as required
# ----------------------------------------------------------------------------------------------------------------------

response.menu = [
    (T('Home'), False, URL('default', 'index'), [])
]
DEVELOPMENT_MENU=False
# ----------------------------------------------------------------------------------------------------------------------
# provide shortcuts for development. you can remove everything below in production
# ----------------------------------------------------------------------------------------------------------------------
if not configuration.get('app.production'):
    _app = request.application
    response.menu += [
        (T('GITHUB'), False, None, [
            (T('Alejandro Morales'), False,
             'https://github.com/alejomora999'),
            (T('Carlos Carreño'), False, 'https://github.com/alejomora999'),
            (T('Juan Arias'), False,
             'https://github.com/alejomora999'),
        ]),
    ]
